# problem7.s
	.set noreorder
	.data

	.text
	.globl main
	.ent main

#void print(int a){
print:
		addi $sp, $sp, -8
		sw $ra, 4($sp)
		sw $a0, 0($sp)
		add $a0, $0, $s0
		ori $v0, $0, 20
		syscall
		lw $ra, 4($sp)
		lw $a0, 0($sp)
		addi $sp, $sp, 8
		jr $ra
		nop
#int sum3(int a, int b, int c){
sum3:
		addi $sp, $sp, -8
		sw $ra, 4($sp)
		sw $s0, 0($sp)
		add $t1, $s1, $s2
	#return a+b+c;
		add $v0, $t1, $t0
		lw $s0, 0($sp)
		lw $ra, 4($sp)
		addi $sp, $sp, 8
		jr $ra
		nop

#int polynomial(int a, int b, int c, int d, int e){
polynomial:
		addi $sp, $sp, -8
		sw $ra, 4($sp)
		sw $s0, 0($sp)
   #x = a << b;
		sll $s1, $a0, $a1
   #y = c << d;
		sll $s2, $a2, $a3
   #z = sum3(x, y, e);
		jal sum3
		nop
		add $s3, $v0, $0
   #print(x);
		add $s0, $s1, $0
		jal print
		nop
   #print(y);
		add $s0, $s2, $0
		jal print
		nop
   #print(z);
		add $s0, $s3, $0
		jal print
		nop
   #return z;
		add $v0, $s3, $0
		lw $s0, 0($sp)
		lw $ra, 4($sp)
		addi $sp, $sp, 8
		jr $ra
		nop
#int main(){
main:
		addi $sp, $sp, -8
		sw $ra, 4($sp)
		sw $s0, 0($sp)
	#int a = 2;
		addi $a0, $0, 2 			#a0 = 2
	#int f = polynomial(a, 3, 4, 5, 6);
		addi $a1, $0, 3			#a1 = 3
		addi $a2, $0, 4 			#a2 = 4
		addi $a3, $0, 5 			#a3 = 5
		addi $t0, $0, 6 			#t0 = 6
		jal polynomial
		nop
		add $s1, $v0, $0			#s1 = v0
   #print(a);
		add $s0, $a0, $0			#s0 = a0 !!!gets set to 150 for some reason
		jal print
		nop
   #print(f);
		add $s0, $s1, $0			#s0 = s1
		jal print
		nop
		addi $v0, $0, 10
		syscall
	.end main

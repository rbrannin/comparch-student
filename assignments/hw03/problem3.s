# problem3.s
   .set noreorder
   .data
	A: .space 32  						#int A[8]
   .text
   .globl main
   .ent main
main:
	#int i;
	#A[0] = 0;
		lui $t0, %hi(A)				#t0 = *A
		ori $t0, $t0, %lo(A)	
		sw	$0, 0($t0)					#*t0 = 0			
	#A[1] = 1;	
		addi $s0, $0, 1				#s0 = 1				
		sw $s0, 4($t0)					#*t0+4 = s0
	#for(i=2; i<8; i++){
		addi $s1, $0, 2					# s1 = 2
loop_cond:
		slti $t1, $s1, 8 					# t1 = (i < 8)
		beq $t1, 0, sequel 				# if (!t1) goto sequel
		nop
	#A[i] = A[i-1] + A[i-2];
		sll $t2, $s1, 2 					#t2 = i*4
		add $t3, $t0, $t2					#t3 = *A[i]
		addi $t4, $t3, -4					#t4 = *A[i-1]
		lw $t4, 0($t4)						#t4 = A[i-1]
		addi $t5, $t3, -8					#t5 = *A[i-2]
		lw $t5, 0($t5)						#t5 = A[i-2]
		add $t6, $t4, $t5					#t6 = t4 + t5
		sw $t6, 0($t3)						#A[i] = t6
	#PRINT_HEX_DEC(A[i]);
		add $a0, $0, $t6 					#a0 = t6
		ori $v0, $0, 20					#v0 = 20
		syscall
		addi $s1, $s1, 1 					# s1 = s1 + 1
		j loop_cond 						# goto loop_cond
		nop	
sequel:  
	#EXIT
		ori $v0, $0, 10					#v0 = 10	
		syscall
	.end main

# problem5.s
	.set noreorder
	.data

	.text
	.globl main
	.ent main

	#typedef struct elt {
   #	int value;
   #	struct elt *next;
	#} elt;


main:
	#elt *head;
	#elt *newelt;
   #newelt = (elt *) MALLOC(sizeof (elt));
	addi $a0, $0, 8		#a0 = 8
	ori $v0, $0, 9			#loading malloc syscall
	syscall
   #newelt->value = 1;
	addi $s0, $0, 1 		#s0 = 1
	sw $s0, 0($v0) 		#*v0 = 1
   #newelt->next = 0;
	sw $0, 4($v0) 			#*v0+4 = 0
   #head = newelt;
	addi $s1, $v0, 0 		#s1 = *v0 
   #newelt = (elt *) MALLOC(sizeof (elt));
	ori $v0, $0, 9			#loading malloc syscall
	syscall					#v0 = new address
   #newelt->value = 2;
	addi $s2, $0, 2 		#s2 = 2
	sw $s2, 0($v0) 		#*v0 = 2
   #newelt->next = head;
	sw $s1, 4($v0) 		#*v0+4 = s1
   #head = newelt;
	add $s1, $0, $v0 		#s1 = v0
   #PRINT_HEX_DEC(head->value);
	lw $a0, 0($s1)
	ori $v0, $0, 20 
	syscall
   #PRINT_HEX_DEC(head->next->value);
	lw $t0, 4($s1)			#t0 = head->next
	lw $a0, 0($t0)			#a0 = head->next->value
	ori $v0, $0, 20
	syscall
	#EXIT;
	ori $v0, $0, 10
	syscall

	.end main

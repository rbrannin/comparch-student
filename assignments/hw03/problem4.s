# problem4.s
	.set noreorder
	.data

	.text
	.globl main
	.ent main

	#	typedef struct record {
   #  	int field0;
   #  	int field1;
	#	} record;

main:
	#record *r = (record *) MALLOC(sizeof(record)); 
		addi $a0, $0, 8		#a0 = 8
		ori $v0, $0, 9			#loading malloc syscall
		syscall			
	#r->field0 = 100;		
		addi $s0, $0, 100		#s0 = 100
		sw $s0, 0($v0)			#*v0 = 100		
	#r->field1 = -1;
		addi $s1, $0, -1 		#s1 = -1
		sw $s1, 4($v0) 		#*v0+4 = -1
	#EXIT;
		ori $v0, $0, 10
		syscall
	.end main

# problem2.s
		.set noreorder
		.data
		A:  .byte   1, 25, 7, 9, -1		#A:  .byte   1, 25, 7, 9, -1
		.text
		.globl main
		.ent main
main:
#int i;
#int current;
#int max;
   	addi $s0, $0, 0						#i = 0;
		lb $s1, A 								#current = A[0];
   	addi $s3, $0, 0						#max = 0;
#while (current > 0) {
loop_cond:
		slt $t0, $0, $s1 						# 0 < current
		beq $t0, $0, sequel 					# if (!t0) goto sequel
		nop
	#if (current > max)
		slt $t1, $s3, $s1
		beq $t1, $0, if_cond
		nop
     	add $s3, $0, $s1 						#max = current;
if_cond:
		addi $s0, $s0, 1						#i = i + 1;
		lui $s4, %hi(A)
		ori $s4, $s4, %lo(A)								
		add $s4, $s4, $s0						
		lb $s1, 0($s4)							#current = A[i];
		j		loop_cond 						#goto loop_cond
		nop
sequel:
		add $a0, $0, $s3  	# PRINT_HEX_DEC(max);
		ori $v0, $0, 20		#print grad in hex
		syscall 				
	
		ori $v0, $0, 10		# EXIT;
		syscall	

	.end main

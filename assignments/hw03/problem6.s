# problem3.s
   .set noreorder
   .data
   
	.text
   .globl main
   .ent main

#void print( int a){
print_value:
		addi $sp, $sp, -8
		sw $ra, 4($sp)
		sw $s0, 0($sp)
		add $a0, $0, $t6 					#a0 = t6
		ori $v0, $0, 20					#v0 = 20
		syscall
		lw $ra, 4($sp)
		lw $s0, 0($sp)
		addi $sp, $sp, 8
		jr $ra
		nop

	#should be implemented with syscall 20

#int main(){
main:
		addi $sp, $sp, -40
		sw $ra, 36($sp)
		sw $s0, 32($sp)
	#int i;
	#A[0] = 0;
		sw	$0, 0($sp)					#*sp = 0			
	#A[1] = 1;	
		addi $s1, $0, 1				#s1 = 1				
		sw $s1, 4($sp)					#*t0+4 = s3
	#for(i=2; i<8; i++){
		addi $s2, $0, 2					# s2 = 2
loop_cond:
		slti $t1, $s2, 8 					# t1 = (s2 < 8)
		beq $t1, 0, sequel 				# if (!t1) goto sequel
		nop
	#A[i] = A[i-1] + A[i-2];
		sll $t2, $s2, 2 					#t2 = s2*4
		add $t3, $sp, $t2					#t3 = *A[i]
		addi $t4, $t3, -4					#t4 = *A[i-1]
		lw $t4, 0($t4)						#t4 = A[i-1]
		addi $t5, $t3, -8					#t5 = *A[i-2]
		lw $t5, 0($t5)						#t5 = A[i-2]
		add $t6, $t4, $t5					#t6 = t4 + t5
		sw $t6, 0($t3)						#A[i] = t6
	#PRINT_HEX_DEC(A[i]);
		jal print_value
		nop
		addi $s2, $s2, 1 					# s2 = s2 + 1
		j loop_cond 						# goto loop_cond
		nop	
sequel:  
	#EXIT
		ori $v0, $0, 10					#v0 = 10	
		syscall
	.end main

# problem1.s
    .set noreorder
    .data

    .text
    .globl main
    .ent main
main: 						
		addi $s0, $0, 84		#score = 84;
		#temp = s1				#int grade; ?!?! what do I do here
#if (score >= 90)
		slt $t0, $s0, 90		# t0 = !(score >= 90) = (x < 90)
		bne $t0, $0, else_if1	# if (t0) goto else_if1
		nop
	#grade = 4;
		addi $s1, $0, 4		#grade = 4
		j sequel 				#goto sequel
		nop

#else if (score >= 80)
else_if1:
		slt $t1, $s0, 80 		# t1 = !(score >= 80) = (x < 80)
		bne $t1, $0, else_if2 	# if (t1) goto else_if2
		nop
	#grade = 3;
		addi $s1, $0, 3		#grade = 3
		j sequel 				#goto sequel
		nop

#else if (score >= 70)
else_if2:
		slt $t2, $s0, 70 		# t2 = !(score >= 70) = (x < 70)
		bne $t2, $0, else_block 	# if  (t2) goto  else_block
		nop
	#grade = 2;
		addi $s1, $0, 2 		#grade = 2
		j sequel 				#goto sequel
		nop
#else
else_block:
	#grade = 0;
		addi $s1, $0, 0

sequel: 							#is there a better way than just putting this blank sequel?
		add $a0, $0, $s1  	# PRINT_HEX_DEC(grade); look into how to print!
		ori $v0, $0, 20		#print grad in hex
		syscall 					# EXIT;

		ori $v0, $0, 10
		syscall

	.end main



